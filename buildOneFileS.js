({
    baseUrl: "js/chat",
    paths: {
        'templates': 'templates',
        "requiremore": "requiremore",
        "hanajquery": "hanajquery",
        "cross-hanajquery": "plugin/hanajquery.XDomainRequest",
        "hanabackbone": "hanabackbone",
        // "hanabackbone.browserStorage":  "plugin/hanabackbone.browserStorage/hanabackbone.browserStorage",
        "localStorage":  "localStorage",
        "hanaunderscore": "hanaunderscore",
        "strophe": "strophe",
        "hana-pluggable": "hana-pluggable",
        "finger": "plugin/fingerprint2.min",
        "base64": "plugin/hanajquery.base64.min",
        "slick": "plugin/slick",
        'text': 'plugin/text',
        'tpl': 'plugin/tpl'
    },
    shim: {
        "hanabackbone": {
            "deps": ["hanaunderscore", "hanajquery"]
            // "exports": "hanabackbone"  //attaches "hanabackbone" to the window object
        },
        "hanaunderscore": {
            exports: "ha_" // exporting _
        },
        "hanajquery": {
            "deps":["requiremore", "libextend"]
        }
    },
    name: "../main",
    mainConfigFile: 'js/main.js',
    preserveLicenseComments: false,
    // Inlines any text! dependencies, to avoid separate requests.
    inlineText: true,
    // Modules to stub out in the optimized file.
    // stubModules: ['hanaunderscore', 'text', 'tpl'],
    // Files combined into a build layer will be removed from the output folder.
    // removeCombined: true,
    out: "built/js/main.js"
})
