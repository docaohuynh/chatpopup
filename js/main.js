var config;
if (typeof(require) === 'undefined') {
    /* XXX: Hack to work around r.js's stupid parsing.
     * We want to save the configuration in a variable so that we can reuse it in
     * tests/main.js.
     */
    require = { // jshint ignore:line
        config: function (c) {
            config = c;
        }
    };
}
require.config({
    baseUrl: 'js/chat',
    waitSeconds: 20,
    paths: {
        'templates': 'templates',
        "requiremore": "requiremore",
        "hanajquery": "hanajquery",
        "cross-hanajquery": "plugin/hanajquery.XDomainRequest",
        "hanajquery-place": "plugin/hanajquery.placeholder",
        "hanabackbone": "hanabackbone",
        // "hanabackbone.browserStorage":  "plugin/hanabackbone.browserStorage/hanabackbone.browserStorage",
        "localStorage":  "localStorage",
        "hanaunderscore": "hanaunderscore",
        "strophe": "strophe",
        "hana-pluggable": "hana-pluggable",
        "finger": "plugin/fingerprint2.min",
        "base64": "plugin/hanajquery.base64.min",
        "slick": "plugin/slick",
        'text': 'plugin/text',
        'tpl': 'plugin/tpl'
    },
    //for traditional libraries not using define() we need to use a shim that allows us to declare them as AMD modules
    shim: {
        "cross-hanajquery": {
            "deps": ["hanajquery"]
        },
        "hanajquery-place": {
            "deps": ["hanajquery"]
        },
        "hanabackbone": {
            "deps": ["hanaunderscore", "hanajquery"]
            // "exports": "hanabackbone"  //attaches "hanabackbone" to the window object
        },
        "hanaunderscore": {
            exports: "ha_" // exporting _
        },
        "hanajquery": {
            exports: "hanajquery" // exporting _
        }
    },

    deps: ['app'],
    urlArgs: "t=20160320000000" //flusing cache, do not use in production
});

