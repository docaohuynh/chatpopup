// hanaCore.js (A browser based XMPP chat client)
// http://hanaCorejs.org
//
// Copyright (c) 2012-2016, Jan-Carel Brand <jc@opkode.com>
// Licensed under the Mozilla Public License (MPLv2)
//
/*global define */

/* This is a hanaCore.js plugin which add support for application-level pings
 * as specified in XEP-0199 XMPP Ping.
 */
(function (root, factory) {
    define("hana-util", [
        "hana-core",
        "hana-api",
        "finger",
        "base64",
        "slick",
        "cross-hanajquery"
    ], factory);
}(this, function (hanaCore, hana_api, finger) {
    "use strict";
    // Strophe methods for building stanzas
    var Strophe = hana_api.env.Strophe;
    // Other necessary globals
    var _ = hana_api.env._,
        ha$iq = hana_api.env.$iq,
        ha$build = hana_api.env.$build,
        ha$msg = hana_api.env.$msg,
        ha$pres = hana_api.env.$pres,
        ha$ = hana_api.env.jQuery;

    ha$.fn.addHyperlinks = function() {
        if (this.length > 0) {
            this.each(function(i, obj) {
                var prot, escaped_url;
                var ha$obj = ha$(obj);
                var x = ha$obj.html();
                var list = x.match(/\b(https?:\/\/|www\.|https?:\/\/www\.)[^\s<]{2,200}\b/g);
                if (list) {
                    for (i = 0; i < list.length; i++) {
                        if ((/\.(gif|jpg|jpeg|tiff|png)ha$/i).test(list[i])) {
                            prot = list[i].indexOf('http://') === 0 || list[i].indexOf('https://') === 0 ? '' : 'http://';
                            escaped_url = encodeURI(decodeURI(list[i])).replace(/[!'()]/g, escape).replace(/\*/g, "%2A");
                            x = x.replace(list[i], '<a style="word-wrap:break-word; white-space:normal; font-size: 14px !important;" href="' + list[i] + '" target="_blank"><img src="' + list[i] + '" class=chat-image /></a>');
                        } else {
                            prot = list[i].indexOf('http://') === 0 || list[i].indexOf('https://') === 0 ? '' : 'http://';
                            escaped_url = encodeURI(decodeURI(list[i])).replace(/[!'()]/g, escape).replace(/\*/g, "%2A");
                            x = x.replace(list[i], '<a style="word-wrap:break-word; white-space:normal; font-size: 14px !important;" target="_blank" rel="noopener" href="' + prot + escaped_url + '">' + list[i] + '</a>');
                        }

                    }
                }
                ha$obj.html(x);
            });
        }
        return this;
    };

    ha$.fn.addEmoticons = function() {
        if (this.length > 0) {
            this.each(function(i, obj) {
                var text = ha$(obj).html();
                // icon :-) :) :] =) :-( :( :[ =( :-P :P :-p :p :-D :D =D -O :O :-o :o  ;) :-/  :/  :-\  :\ :* -_- B) O:) <3 (y)
                text = text.replace(/:\*/g, '<i class="em em-hana-kiss"></i>');
                text = text.replace(/\-\_\-/g, '<i class="em em-hana-squint"></i>');
                text = text.replace(/8\)/g, '<i class="em em-hana-glasses"></i>');
                text = text.replace(/B\)/g, '<i class="em em-hana-glasses"></i>');
                text = text.replace(/O:\)/g, '<i class="em em-hana-angel"></i>');
                text = text.replace(/3:\)/g, '<i class="em em-hana-devil"></i>');
                text = text.replace(/:\)/g, '<i class="em em-hana-smile"></i>');
                text = text.replace(/:\]/g, '<i class="em em-hana-smile"></i>');
                text = text.replace(/:\-\)/g, '<i class="em em-hana-smile"></i>');
                text = text.replace(/=\)/g, '<i class="em em-hana-smile"></i>');
                text = text.replace(/:\(/g, '<i class="em em-hana-sad"></i>');
                text = text.replace(/:\-\(/g, '<i class="em em-hana-sad"></i>');
                text = text.replace(/=\(/g, '<i class="em em-hana-sad"></i>');
                text = text.replace(/:P/g, '<i class="em em-hana-tongue"></i>');
                text = text.replace(/:\-P/g, '<i class="em em-hana-tongue"></i>');
                text = text.replace(/:p/g, '<i class="em em-hana-tongue"></i>');
                text = text.replace(/:\-p/g, '<i class="em em-hana-tongue"></i>');
                text = text.replace(/:D/g, '<i class="em em-hana-grin"></i>');
                text = text.replace(/:\-D/g, '<i class="em em-hana-grin"></i>');
                text = text.replace(/=D/g, '<i class="em em-hana-grin"></i>');
                text = text.replace(/:O/g, '<i class="em em-hana-gasp"></i>');
                text = text.replace(/:\-O/g, '<i class="em em-hana-gasp"></i>');
                text = text.replace(/:o/g, '<i class="em em-hana-gasp"></i>');
                text = text.replace(/;\)/g, '<i class="em em-hana-wink"></i>');
                text = text.replace(/;\-\)/g, '<i class="em em-hana-wink"></i>');
                text = text.replace(/:\-\//g, '<i class="em em-hana-unsure"></i>');
                text = text.replace(/:\-\\/g, '<i class="em em-hana-unsure"></i>');
                text = text.replace(/:\\/g, '<i class="em em-hana-unsure"></i>');
                text = text.replace(/&lt;3/g, '<i class="em em-hana-heart"></i>');
                text = text.replace(/\(y\)/g, '<i class="em em-hana-like"></i>');
                ha$(obj).html(text);
            });
        }
        return this;
    };

    hana_api.plugins.add('hana-util', {

        initialize: function () {
            hanaCore.sendPost = function (url, body, callBack) {
                // console_bk.log('URLLL ' + url);
                // console_bk.log('BODY  ' + body + ' ' + window.is_ie);
                if (window.is_ie) {
                    url = hanaCore.hana_apiserverie + url;
                    var appliance = new window.XDomainRequest();
                    appliance.onprogress = function() {}; // no aborting
                    appliance.ontimeout = function() {}; // "
                    appliance.onload = function() {
                        callBack(JSON.parse(appliance.responseText));
                    };
                    appliance.onerror = function() {
                        callBack(false);
                    };
                    appliance.open("POST", url, true);
                    appliance.send(body);
                } else {
                    ha$.ajax({
                        url: hanaCore.hana_apiserver + url,
                        type: "POST",
                        crossDomain: true,
                        data: body,
                        contentType: "application/json",
                        success: function (result) {
                            // console_bk.log('RESULT  ' + result);
                            callBack(result);
                        },
                        error: function (result) {
                            // console_bk.log('RESULTFAIL  ' + JSON.stringify(result));
                            callBack(false);
                        }
                    });
                }



            };
            hanaCore.sendGet = function (url, callBack) {
                ha$.ajax({
                    url: url,
                    type: 'GET',
                    // crossDomain: true,
                    dataType: 'html',
                    success: function (code_html, textStatus, jqXHR) { // code_html contient le HTML renvoyé
                        var statusCode = jqXHR.status;
                        callBack(code_html, statusCode);
                    }
                });
            };
            hanaCore.appendCssHead = function () {
                if (hanaCore.hana_product === 'dev') {
                    ha$('head').append(ha$('<link rel="stylesheet" type="text/css" />').attr('href', hanaCore.resource_popup+'/css/hana.css'));
                } else {
                    ha$('head').append(ha$('<link rel="stylesheet" type="text/css" />').attr('href', hanaCore.resource_popup+'/css/minimize/hana.css'));
                }

            };
            hanaCore.checkRegexp = function(queryToEscape, regex) {
                return regex.test(queryToEscape);
            };
            hanaCore.validEmail = function (value) {
                if (hanaCore.checkRegexp(value, /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()\.,;\s@\"]+\.{0,1})+[^<>()\.,;:\s@\"]{2,})$/)) {
                    return true;
                } else {
                    return false;
                }
            };
            hanaCore.regexReplaceEnter = function(string) {
                var rege = /(\\r\\n)+/g;
                var string = string.replace(rege, '\\r\\n');
                var rege1 = /(\\r)+/g;
                string = string.replace(rege1, '\\r');
                var rege1 = /(\\n)+/g;
                string = string.replace(rege1, '\\n');
                var rege2 = /( )+/g;
                string = string.replace(rege2, ' ');
                return string;
            };
            hanaCore.getUniqueUser = function (callBack) {
                new finger().get(function (result, components) {
                    callBack(result);
                });
            };
            hanaCore.base64Decode = function (string) {
                var decodedString = Base64.decode(string);
                return decodedString;
            };
            hanaCore.base64Encode = function (string) {
                var decodedString = Base64.encode(string);
                return decodedString;
            };
            hanaCore.SendMsgRaw = function (msg) {
                hanaCore.connection.send(msg);
            };
            hanaCore.renderFinger = function (callBack) {
                new finger().get(function (result, components) {
                    // console_bk.log('FINGER ' + JSON.stringify(result));
                    callBack(result);
                });
            };
            hanaCore.checkResponesive = function () {
                if(document.querySelector('meta[name="viewport"][content*="width=device-width"]') !== null){
                    hanaCore.viewportSite =   document.querySelector('meta[name="viewport"][content*="width=device-width"]');
                    return true;
                } else if (document.querySelector('meta[name="viewport"]') !== null){
                    hanaCore.viewportSite =   document.querySelector('meta[name="viewport"]');
                    return true;
                } else {
                    return false;
                }
            };
            hanaCore.turnOnDeviceViewport = function () {
                ha$('body').addClass('hana-mobile-body');
                this.removeMetaIfExist();
                var viewportMeta = document.createElement('meta');
                viewportMeta.setAttribute('id', 'myMeta');
                viewportMeta.setAttribute('name', 'viewport');
                viewportMeta.setAttribute('content', 'width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0');
                document.head.appendChild(viewportMeta);
            };
            hanaCore.turnOffDeviceViewport = function () {
                ha$('body').removeClass('hana-mobile-body');
                if (hanaCore.is_responesive) {
                    this.removeMetaIfExist();
                    document.head.appendChild(hanaCore.viewportSite);
                    return;
                } else if (hanaCore.is_responesive === false ) {

                } else {
                    if (this.checkResponesive()) {
                        return;
                    }
                }
                this.removeMetaIfExist();
                var viewportMeta = document.createElement('meta');
                viewportMeta.setAttribute('id', 'myMeta');
                viewportMeta.setAttribute('name', 'viewport');
                viewportMeta.setAttribute('content', 'width=980, user-scalable=yes');
                document.head.appendChild(viewportMeta);
            };
            hanaCore.removeMetaIfExist = function () {
                var metaElement = document.querySelector('meta[name="viewport"][content*="width=device-width"]');
                if (metaElement) document.head.removeChild(metaElement);
            };
            hanaCore.checkwindownvisible = function (callback) {
                // Set the name of the hidden property and the change event for visibility
                var hidden, visibilityChange;
                if (typeof document.hidden !== "undefined") { // Opera 12.10 and Firefox 18 and later support
                    hidden = "hidden";
                    visibilityChange = "visibilitychange";
                } else if (typeof document.msHidden !== "undefined") {
                    hidden = "msHidden";
                    visibilityChange = "msvisibilitychange";
                } else if (typeof document.webkitHidden !== "undefined") {
                    hidden = "webkitHidden";
                    visibilityChange = "webkitvisibilitychange";
                }
                // If the page is hidden, pause the video;
                // if the page is shown, play the video
                function handleVisibilityChange() {
                    if (document[hidden]) {
                        callback('hidden');
                    } else {
                        callback('focus');
                    }
                }
                // Warn if the browser doesn't support addEventListener or the Page Visibility API
                if (typeof document.addEventListener === "undefined" || typeof document[hidden] === "undefined") {
                    console.log("This demo requires a browser, such as Google Chrome or Firefox, that supports the Page Visibility API.");
                } else {
                    // Handle page visibility change
                    document.addEventListener(visibilityChange, handleVisibilityChange, false);

                }
            };

            hanaCore.countWords = function (s) {
                s = s.replace(/(^\s*)|(\s*$)/gi,"");//exclude  start and end white-space
                s = s.replace(/[ ]{2,}/gi," ");//2 or more space to 1
                s = s.replace(/\n /,"\n"); // exclude newline with a start spacing
                return s.split(' ').length;
            };

            hanaCore.getTitleHtml = function (s) {
                hanaCore.pageTitleHtml = ha$("title").text();
            };

            hanaCore.checkMobile = function () {
                var device = 'desktop';
                var ua = navigator.userAgent;
                var checker = {
                    iphone: ua.match(/(iPhone|iPod|iPad)/),
                    blackberry: ua.match(/BlackBerry/),
                    android: ua.match(/Android/),
                    windowphone: ua.match(/Windows Phone/),
                    webos: ua.match(/webOS/)
                };
                if (checker.android) {
                    device = 'mobile';
                } else if (checker.iphone) {
                    device = 'mobile';
                } else if (checker.blackberry) {
                    device = 'mobile';
                } else if (checker.windowphone) {
                    device = 'mobile';
                } else if (checker.webos) {
                    device = 'mobile';
                } else {
                    //day la desktop
                    var ha$Width_window = ha$(window).width();
                    var ha$Width_mobile = 768;
                    if (ha$Width_mobile > ha$Width_window) {
                        device = 'mobile';
                    } else {
                        device = 'desktop';
                    }

                }
                return device;
            };
            hanaCore.generateResource = function () {
                return '10min' + '/webclient_' + Math.floor(Math.random() * 139749825).toString();
            };

            // console_bk.log("REGISTER HANA UTIL...");

        }
    });
}));
