define([
    "hanajquery",
    "hana-api",
    "hanabackbone",
    // "backbone.browserStorage",
    "localStorage",
    "hanaunderscore",
    "libextend",
    // "login",
    "hana-ping",
    "hana-chat"
], function ($, hanaApi,  Backbone, _) {
    'use strict';

    window.hanaapi = hanaApi;
    hanaApi.initialize();
    return hanaApi;

});