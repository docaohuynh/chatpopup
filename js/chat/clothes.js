define([
    "backbone",
    "underscore"
], function (Backbone, _) {

    'use strict';

    return Backbone.Model.extend(
        {
            getClothes: function () {
                return "<span class='shop-clothes-button'> skirt,shoes,socks,hat</span>";
            }
        });
});
