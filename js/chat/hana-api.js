// HanaCore.js (A browser based XMPP chat client)
// http://HanaCorejs.org
//
// Copyright (c) 2012-2016, Jan-Carel Brand <jc@opkode.com>
// Licensed under the Mozilla Public License (MPLv2)
//
/*global define */
/*
window.HanaCustom = {};
HanaCustom.setUserInfoData = {};
*/
/* This is a HanaCore.js plugin which add support for multi-user chat rooms, as
 * specified in XEP-0045 Multi-user chat.
 */
(function (root, factory) {
    define("hana-api", [
        "hanajquery",
        "hanaunderscore",
        "strophe",
        "hana-core"
    ],
        factory);
}(this, function (ha$, ha_, strophe, HanaCore) {
    var Strophe = strophe.Strophe;
    Strophe.addConnectionPlugin('iexdomain', {
        init: function (conn) {
            // replace Strophe.Request._newXHR with new IE CrossDomain version
            var nativeXHR = new XMLHttpRequest();
            if (window.XDomainRequest && !("withCredentials" in nativeXHR)) {
                if (window.attachEvent && !window.addEventListener) {
                    window.notsupport = true;
                }
                window.is_ie = true;
                Strophe.Request.prototype._newXHR = function () {
                    var xhr = new XDomainRequest();
                    xhr.setRequestHeader = function () { };
                    xhr.readyState = 0;

                    xhr.onreadystatechange = this.func.bind(undefined, this);
                    xhr.onerror = function () {
                        xhr.readyState = 4;
                        xhr.status = 500;
                        xhr.onreadystatechange(xhr.responseText);
                    };
                    xhr.ontimeout = function () {
                        xhr.readyState = 4;
                        xhr.status = 0;
                        xhr.onreadystatechange(xhr.responseText);
                    };
                    xhr.onload = function () {
                        xhr.readyState = 4;
                        xhr.status = 200;
                        var _response = xhr.responseText;
                        var _xml = new ActiveXObject('Microsoft.XMLDOM');
                        _xml.async = 'false';
                        _xml.loadXML(_response);
                        xhr.responseXML = _xml;
                        xhr.onreadystatechange(_response);
                    };
                    return xhr;
                };

                window.ieFormData = function ieFormData() {
                    if (window.FormData == undefined) {
                        this.processData = true;
                        this.contentType = 'application/x-www-form-urlencoded';
                        this.append = function (name, value) {
                            this[name] = value == undefined ? "" : value;
                            return true;
                        }
                    }
                    else {
                        var formdata = new FormData();
                        formdata.processData = false;
                        formdata.contentType = false;
                        return formdata;
                    }
                }
            } else {
                // console.info("Browser doesnt support XDomainRequest." + "  Falling back to native XHR implementation.");
            }
        }
    });
    var nativeXHR = new XMLHttpRequest();
    if (window.XDomainRequest && !("withCredentials" in nativeXHR)) {
        window.is_ie = true;
        window.httpPost = function (callBack) {
            var xhr = new XDomainRequest();
            xhr.setRequestHeader = function () { };
            xhr.readyState = 0;

            // xhr.onreadystatechange = this.func.bind(undefined, this);
            xhr.onreadystatechange = callBack();
            xhr.onerror = function () {
                xhr.readyState = 4;
                xhr.status = 500;
                xhr.onreadystatechange(xhr.responseText);
            };
            xhr.ontimeout = function () {
                xhr.readyState = 4;
                xhr.status = 0;
                xhr.onreadystatechange(xhr.responseText);
            };
            xhr.onload = function () {
                xhr.readyState = 4;
                xhr.status = 200;
                var _response = xhr.responseText;
                var _xml = new ActiveXObject('Microsoft.XMLDOM');
                _xml.async = 'false';
                _xml.loadXML(_response);
                xhr.responseXML = _xml;
                xhr.onreadystatechange(_response);
            };
            return xhr;
        };
    } else {
        // console.info("Browser doesnt support XDomainRequest." + "  Falling back to native XHR implementation.");
    }
    return {
        'initialize': function (settings, callback) {
            return HanaCore.initialize(settings, callback);
        },
        'start': function () {
            HanaCore.logIn('web_2824099c-f6f9-43bd-ae4f-43c19c483054hanahana166b8642edda5ab859f98eafffc3c122', '6665');
            // HanaCore.renderFinger();
            // var enString = HanaCore.base64Encode(" toi la ai nhi ");
            // console.log(enString);
            // console.log(HanaCore.base64Decode(enString));
            // HanaCore.slickSlide();
        },
        'connection': {
            'connected': function () {
                return HanaCore.connection && HanaCore.connection.connected || false;
            },
            'disconnect': function () {
                HanaCore.connection.disconnect();
            },
        },
        'hana_api': {
            'setUserInfo': function (users) {
                // {name: name, email: email, phone: phone, address: address}
                HanaCore.setUserInfo = true;
                HanaCore.setUserInfoData = users;
                HanaCore.isGetcustomerinfo = true;

            }
        },
        'user': {
            'jid': function () {
                return HanaCore.connection.jid;
            },
            'login': function (credentials) {
                HanaCore.initConnection();
                HanaCore.logIn(credentials);
            },
            'logout': function () {
                HanaCore.logOut();
            },
            'status': {
                'get': function () {
                    return HanaCore.xmppstatus.get('status');
                },
                'set': function (value, message) {
                    var data = { 'status': value };
                    if (!ha_.contains(ha_.keys(HanaCore.STATUS_WEIGHTS), value)) {
                        throw new Error('Invalid availability value. See https://xmpp.org/rfcs/rfc3921.html#rfc.section.2.2.2.1');
                    }
                    if (typeof message === "string") {
                        data.status_message = message;
                    }
                    HanaCore.xmppstatus.sendPresence(value);
                    HanaCore.xmppstatus.save(data);
                },
                'message': {
                    'get': function () {
                        return HanaCore.xmppstatus.get('status_message');
                    },
                    'set': function (stat) {
                        HanaCore.xmppstatus.save({ 'status_message': stat });
                    }
                }
            },
        },
        'settings': {
            'get': function (key) {
                if (ha_.contains(Object.keys(HanaCore.default_settings), key)) {
                    return HanaCore[key];
                }
            },
            'set': function (key, val) {
                var o = {};
                if (typeof key === "object") {
                    ha_.extend(HanaCore, ha_.pick(key, Object.keys(HanaCore.default_settings)));
                } else if (typeof key === "string") {
                    o[key] = val;
                    ha_.extend(HanaCore, ha_.pick(o, Object.keys(HanaCore.default_settings)));
                }
            }
        },
        'contacts': {
            'get': function (jids) {
                var _transform = function (jid) {
                    var contact = HanaCore.roster.get(Strophe.getBareJidFromJid(jid));
                    if (contact) {
                        return contact.attributes;
                    }
                    return null;
                };
                if (typeof jids === "undefined") {
                    jids = HanaCore.roster.pluck('jid');
                } else if (typeof jids === "string") {
                    return _transform(jids);
                }
                return ha_.map(jids, _transform);
            },
            'add': function (jid, name) {
                if (typeof jid !== "string" || jid.indexOf('@') < 0) {
                    throw new TypeError('contacts.add: invalid jid');
                }
                HanaCore.roster.addAndSubscribe(jid, ha_.isEmpty(name) ? jid : name);
            }
        },
        'chats': {
            'open': function (jids) {
                var chatbox;
                if (typeof jids === "undefined") {
                    HanaCore.log("chats.open: You need to provide at least one JID", "error");
                    return null;
                } else if (typeof jids === "string") {
                    chatbox = HanaCore.wrappedChatBox(HanaCore.chatboxes.getChatBox(jids, true));
                    chatbox.open();
                    return chatbox;
                }
                return ha_.map(jids, function (jid) {
                    chatbox = HanaCore.wrappedChatBox(HanaCore.chatboxes.getChatBox(jid, true));
                    chatbox.open();
                    return chatbox;
                });
            },
            'get': function (jids) {
                if (typeof jids === "undefined") {
                    var result = [];
                    HanaCore.chatboxes.each(function (chatbox) {
                        // FIXME: Leaky abstraction from MUC. We need to add a
                        // base type for chat boxes, and check for that.
                        if (chatbox.get('type') !== 'chatroom') {
                            result.push(HanaCore.wrappedChatBox(chatbox));
                        }
                    });
                    return result;
                } else if (typeof jids === "string") {
                    return HanaCore.wrappedChatBox(HanaCore.chatboxes.getChatBox(jids, true));
                }
                return ha_.map(jids,
                    ha_.partial(
                        ha_.compose(
                            HanaCore.wrappedChatBox.bind(HanaCore), HanaCore.chatboxes.getChatBox.bind(HanaCore.chatboxes)
                        ), _, true
                    )
                );
            }
        },
        'tokens': {
            'get': function (id) {
                if (!HanaCore.expose_rid_and_sid || typeof HanaCore.connection === "undefined") {
                    return null;
                }
                if (id.toLowerCase() === 'rid') {
                    return HanaCore.connection.rid || HanaCore.connection._proto.rid;
                } else if (id.toLowerCase() === 'sid') {
                    return HanaCore.connection.sid || HanaCore.connection._proto.sid;
                }
            }
        },
        'listen': {
            'once': function (evt, handler) {
                HanaCore.once(evt, handler);
            },
            'on': function (evt, handler) {
                HanaCore.on(evt, handler);
            },
            'not': function (evt, handler) {
                HanaCore.off(evt, handler);
            },
            'stanza': function (name, options, handler) {
                if (typeof options === 'function') {
                    handler = options;
                    options = {};
                } else {
                    options = options || {};
                }
                HanaCore.connection.addHandler(
                    handler,
                    options.ns,
                    name,
                    options.type,
                    options.id,
                    options.from,
                    options
                );
            },
        },
        'send': function (stanza) {
            // console.log('SEND MESSAGE ' + stanza);
            HanaCore.connection.send(stanza);
        },
        'plugins': {
            'add': function (name, plugin) {
                plugin.__name__ = name;
                HanaCore.pluggable.plugins[name] = plugin;
            },
            'remove': function (name) {
                delete HanaCore.plugins[name];
            },
            'override': function (name, value) {
                /* Helper method for overriding methods and attributes directly on the
                 * HanaCore object. For Backbone objects, use instead the 'extend'
                 * method.
                 *
                 * If a method is overridden, then the original method will still be
                 * available via the _super attribute.
                 *
                 * name: The attribute being overridden.
                 * value: The value of the attribute being overridden.
                 */
                HanaCore._overrideAttribute(name, value);
            },
            'extend': function (obj, attributes) {
                /* Helper method for overriding or extending HanaCore's Backbone Views or Models
                 *
                 * When a method is overriden, the original will still be available
                 * on the _super attribute of the object being overridden.
                 *
                 * obj: The Backbone View or Model
                 * attributes: A hash of attributes, such as you would pass to Backbone.Model.extend or Backbone.View.extend
                 */
                HanaCore._extendObject(obj, attributes);
            }
        },
        'env': {
            '$build': strophe.$build,
            '$iq': strophe.$iq,
            '$msg': strophe.$msg,
            '$pres': strophe.$pres,
            'Strophe': strophe.Strophe,
            'b64_sha1': strophe.SHA1.b64_sha1,
            'ha_': ha_,
            'jQuery': ha$,
        }
    };
}));
