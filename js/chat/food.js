define([
    "backbone",
    "underscore"
], function (Backbone, _) {

    'use strict';

    return Backbone.Model.extend(
        {
            getFoods: function () {
                return "<span class='shop-food-button'> pizza,hamburger,hamburger-pizza </span>";
            }
        });
});
