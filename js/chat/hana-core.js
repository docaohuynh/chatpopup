// hanaCore.js (A browser based XMPP chat client)
// http://conversejs.org
//
// Copyright (c) 2012-2016, Jan-Carel Brand <jc@opkode.com>
// Licensed under the Mozilla Public License (MPLv2)
//
/*global Backbone, define, window, document, locales */

(function(root, factory) {
    // Two modules are loaded as dependencies.
    //
    // * **converse-dependencies**: A list of dependencies hanaCore.js depends on.
    //   The path to this module is in main.js and the module itself can be overridden.
    // * **converse-templates**: The HTML templates used by hanaCore.js.
    //
    // The dependencies are then split up and passed into the factory function,
    // which contains and instantiates hanaCore.js.
    define("hana-core", [
        "hanajquery",
        "hanaunderscore",
        "strophe",
        "hana-pluggable",
        "base64",
        "slick"
    ], factory);
}(this, function(ha$,  ha_, Strophe, pluggable) {
    // Strophe globals
    var ha$build = Strophe.$build;
    var ha$iq = Strophe.$iq;
    var ha$msg = Strophe.$msg;
    var ha$pres = Strophe.$pres;
    var b64_sha1 = Strophe.SHA1.b64_sha1;
    Strophe = Strophe.Strophe;


    var event_context = {};
    
    var hanaCore = {
        emit: function(evt, data) {
            ha$(event_context).trigger(evt, data);
        },
        once: function(evt, handler) {
            ha$(event_context).one(evt, handler);
        },

        on: function(evt, handler) {
            if (ha_.contains(['ready', 'initialized'], evt)) {
                hanaCore.log('Warning: The "' + evt + '" event has been deprecated and will be removed, please use "connected".');
            }
            ha$(event_context).bind(evt, handler);
        },
        off: function(evt, handler) {
            ha$(event_context).unbind(evt, handler);
        }
    };

    pluggable.enable(hanaCore);
    hanaCore.initialize = function(settings, callback) {
        "use strict";
        var init_deferred = new ha$.Deferred();
        // console_bk.log("INITTTTTT...");
        var hanaCore = this;
        this.user_settings = settings; // Save the user settings so that they can be used by plugins
        this.DIVIDENT_CHAT = {
            'sendall': 0,
            'dividend': 1
        };
        this.quickreplies = [];
        this.STATEMESSAGE = { // state message
            'NOTSENT': 0,
            'HASSENT': 1,
            'HASRECEIVED': 2,
            'HASREAD': 3
        };

        hanaCore.dataObject = new Object();
          /*this.default_settings = {
            all_agent: {},
            hana_apiserver: 'https://test.mideasvn.com:8200/ReengBackendBiz/',
            hana_apiserverie: 'https://test.mideasvn.com:8206/ReengBackendBiz/',
            hana_bosh: 'https://test.mideasvn.com:8281/http-bind',
            partner_id: 12,
            file_path: 'https://test.mideasvn.com:9000',
            resource_popup: 'https://test.mideasvn.com:8000/popup/poptemp',
            hana_room: '@muc.10min',
            hana_product: 'dev',
            hana_appid: 'xxx###xxx',
            hana_checkconnect: 'https://connect.hana.ai',
            hana_collapse: 0,
            sky_hanaprefix: 'hanaagenthana_',
            sky_myname: 'Tôi',
            storage: 'session',
            hana_hanaprefix: 'hanaagenthana_',
            hana_resource_api: 'https://resources.mideasvn.com',
            hana_upload_api: 'https://test.mideasvn.com:8206/api/upload',
            hana_nginx_domain: 'https://test.mideasvn.com:8000'
        };*/
          /*this.default_settings = {
            all_agent: {},
            hana_apiserver: 'https://test.mideasvn.com:8200/ReengBackendBiz/',
            hana_apiserverie: 'https://test.mideasvn.com:8206/ReengBackendBiz/',
            hana_bosh: 'https://test.mideasvn.com:8281/http-bind',
            partner_id: 12,
            file_path: 'https://test.mideasvn.com:9000',
            resource_popup: 'https://test.mideasvn.com:8000/popup/poptemp',
            hana_room: '@muc.10min',
            hana_product: 'dev',
            hana_appid: '2824099c-f6f9-43bd-ae4f-43c19c483054',
            // hana_appid: '051f53e0-ea71-40e0-a08f-bb7ecd8d9fd7', //nhung
            hana_checkconnect: 'https://connect.hana.ai',
            hana_collapse: 0,
            sky_hanaprefix: 'hanaagenthana_',
            sky_myname: 'Tôi',
            storage: 'session',
            hana_hanaprefix: 'hanaagenthana_',
            hana_resource_api: 'https://resources.mideasvn.com',
            hana_upload_api: 'https://test.mideasvn.com:8206/api/upload',
            hana_nginx_domain: 'https://test.mideasvn.com:8000'
        };*/

         this.default_settings = {
            all_agent: {},
            hana_apiserver: 'https://chatapi.mideasvn.com/ReengBackendBiz/',
            hana_apiserverie: 'https://resources.mideasvn.com/ReengBackendBiz/',
            hana_bosh: 'https://chatweb.mideasvn.com/http-bind',
            partner_id: 12,
            file_path: 'https://file.mideasvn.com',
            resource_popup: 'https://file.mideasvn.com/10min/popup',
            hana_room: '@muc.10min',
            hana_product: 'prod',
            hana_appid: 'xxx###xxx',
            hana_checkconnect: 'https://connect.hana.ai',
            hana_collapse: 0,
            sky_hanaprefix: 'hanaagenthana_',
            sky_myname: 'Tôi',
            storage: 'session',
            hana_hanaprefix: 'hanaagenthana_',
            hana_resource_api: 'https://resources.mideasvn.com',
            hana_upload_api: 'https://resources.mideasvn.com/api/upload',
            hana_nginx_domain: 'https://file.mideasvn.com'
        };
         /*this.default_settings = {
            all_agent: {},
            hana_apiserver: 'https://chatapi.mideasvn.com/ReengBackendBiz/',
            hana_apiserverie: 'https://resources.mideasvn.com/ReengBackendBiz/',
            hana_bosh: 'https://chatweb.mideasvn.com/http-bind',
            partner_id: 12,
            file_path: 'https://file.mideasvn.com',
            resource_popup: 'https://file.mideasvn.com/10min/popup',
            hana_room: '@muc.10min',
            hana_product: 'prod',
            // hana_appid: 'df7a8b50-4442-11e7-9341-09d921074e88',
            hana_appid: 'c7b52a94-97c4-4418-a3bb-ada10106985d',
            hana_checkconnect: 'https://connect.hana.ai',
            hana_collapse: 0,
            sky_hanaprefix: 'hanaagenthana_',
            sky_myname: 'Tôi',
            storage: 'session',
            hana_hanaprefix: 'hanaagenthana_',
            hana_resource_api: 'https://resources.mideasvn.com',
            hana_upload_api: 'https://resources.mideasvn.com/api/upload',
            hana_nginx_domain: 'https://file.mideasvn.com'
        };*/
        //API
        if (window.HanaCustom) {
            if (HanaCustom.setUserInfoData) {
                hanaCore.setUserInfo = true;
                hanaCore.setUserInfoData = HanaCustom.setUserInfoData;
                hanaCore.isGetcustomerinfo = true;
            }
        }
        ha_.extend(this, this.default_settings);
        ha_.extend(this, ha_.pick(settings, Object.keys(this.default_settings)));
        this.base64Decode = function (string) {
            var decodedString = Base64.decode(string);
            return decodedString;
        };
        this.base64Encode = function (string) {
            var decodedString = Base64.encode(string);
            return decodedString;
        };
        this.slickSlide = function () {
            ha$('.your-class').slick({
                slidesToShow: 1,
                slidesToScroll: 1,
                arrows: false,
                fade: true
            });
        }
        this.log = function (msg, data) {
            if (hanaCore.hana_product === 'dev') {
                console.log(msg + ' ' + data);
            }

        }
        this.rawInput = function (data) {
            // hanaCore.log('RECV Nhan message ', data);
            hanaCore.emit("messagereceive", data);
        };
        this.rawOutput = function (data) {
            // hanaCore.log('SEND Send message ', data);
        };
        this.onConnect = function (status) {
            if (status == Strophe.Status.CONNECTING) {
                hanaCore.log('Strophe is connecting.');
            } else if (status == Strophe.Status.CONNFAIL) {
                hanaCore.log('Strophe failed to connect.');
            } else if (status == Strophe.Status.DISCONNECTING) {
                hanaCore.log('Strophe is disconnecting.');
            } else if (status == Strophe.Status.DISCONNECTED) {
                hanaCore.log('Strophe is disconnected.');
                // hanaCore.logIn(hanaCore.usernamechat, hanaCore.passchat);
            } else if (status == Strophe.Status.CONNECTED) {
                hanaCore.log('Strophe is connected.');
                hanaCore.emit("connected");
                // Strophe.logout();
            }
        };
        this.onMessageReceive = function (stanza) {

            return true;
        };
        this.logIn = function(users, pass){
            users = users + '@' + hanaCore.generateResource();

            var BOSH_SERVICE = hanaCore.hana_bosh;
            this.connection = null;
            this.connection = new Strophe.Connection(BOSH_SERVICE);
            this.connection.rawInput = this.rawInput;
            this.connection.rawOutput = this.rawOutput;
            this.jid = users;
            hanaCore.bare_jid = Strophe.getBareJidFromJid(users);
            hanaCore.node_jid = Strophe.getNodeFromJid(users);
            hanaCore.resource = Strophe.getResourceFromJid(users);
            hanaCore.domain = Strophe.getDomainFromJid(users);
            // console_bk.log('User and pass ' + users + ' pas ' + pass);
            this.connection.connect(users, pass, hanaCore.onConnect);
            this.connection.addHandler(hanaCore.onMessageReceive.bind(this), null, 'message', 'groupchat');
        };

        var updateSettings = function(settings) {
            /* Helper method which gets put on the plugin and allows it to
             * add more user-facing config settings to hanaCore.js.
             */
            ha_.extend(hanaCore.default_settings, settings);
            ha_.extend(hanaCore, settings);
            hanaCore.user_settings = {
            }
            ha_.extend(hanaCore, ha_.pick(hanaCore.user_settings, Object.keys(settings)));
        };
        hanaCore.pluggable.initializePlugins({
            'updateSettings': updateSettings,
            'hanaCore': hanaCore
        });

        return init_deferred.promise();

    };
    return hanaCore;
}));