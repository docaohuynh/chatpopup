define([
    "backbone",
    "underscore"
], function (Backbone, _) {

    'use strict';

    return Backbone.Model.extend(
        {
            getDestinations: function () {
                return  "<span class='shop-travel-button'> new york,san francisco,london,paris,namibia,japan,oviedo,buiza,buenos aires</span>";
            }
        });
});
